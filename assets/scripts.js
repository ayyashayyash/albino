
_indebug = true;
function _attn(o, message) {
	if (window && window._indebug) {
		console.log('%c ' + message, 'background: orange; font-weight: bold', o);
	}
}



window.addEventListener('load', () => {

	document.querySelector('.start')?.addEventListener('click', () =>{ 

		const progress = document.querySelector('#progress');
		const progressLabel = document.querySelector('#progresslabel');

		setTimeout(() => {
			progress.value = '30';
			progressLabel.textContent = 'Fetching building data...';
		}, 1000);

		setTimeout(() => {
			progress.value = '60';
			progressLabel.textContent = 'Getting base reactions...';
		}, 2000);
		setTimeout(() => {
			progress.value = '80';
			progressLabel.textContent = 'Creating base plan...';
		}, 3000);
		setTimeout(() => {
			progress.value = '98';
			progressLabel.textContent = 'Finalizing...';
		}, 4000);
		setTimeout(() => {
			progress.value = '100';
			window.location.href = 'second.html';

		}, 5000);
	});

	document.getElementById('colorSelector')?.addEventListener('click', (event) => {
		// change color of circles, hue value only
		const circles = document.querySelector('#planeView').querySelectorAll('circle');
		const hue = Math.floor(Math.random() * 256);

		circles.forEach((n, index) => {
			n.style.fill = `hsla(${hue}, ${ Math.random() * 100 }%, 50%, 1)`;
		});
	});

	document.getElementById('continue')?.addEventListener('click', (event) => {
		// lets pop few screens before moving to second stage
		// window.location.href = 'project.html';
		const modal = event.target.closest('.modal-overlay').shModal;
		
		modal.hide();


	});
});

