const config = require('./shut.config.json');
const gulpBin = require('../../../work/gulpbin/gulpfile.js')(config);

Object.keys(gulpBin).forEach(i => {
	exports[i] = gulpBin[i];
});
